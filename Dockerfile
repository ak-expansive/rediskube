FROM redis:5.0.5

LABEL maintainer="adam_kirk@live.co.uk"

ENV HOOK_ROOT /opt/rediskube

RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y jq

# Remove vim; for testing
RUN apt-get install -y vim

RUN mkdir /opt/rediskube
COPY ./hooks/ /opt/rediskube/
RUN chmod +x /opt/rediskube/*.sh
