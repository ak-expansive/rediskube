#!/bin/bash

# For API Connection
K8S_HOST=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
CACERT=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)

# For Api Pod Filters
POD_FILTER_LABEL=${POD_FILTER_LABEL:-name}
POD_FILTER_VALUE=${POD_FILTER_VALUE:-rediskube}

# Retry loops config
WAIT_INTERVAL=${WAIT_INTERVAL:-10}
MAX_PING_COUNT=${MAX_PING_COUNT:-10}
MAX_CLUSTER_CHECK_COUNT=${MAX_CLUSTER_CHECK_COUNT:-10}

# Required Env Vars
# REDIS_STS_NAME: The unique name of the stateful set in this namespace
# POD_IP: The IP of this pod

# Useful functions
function authedredis()
{
	redis-cli -a $REDIS_PASSWORD --no-auth-warning "$@"
}

function get_node_role()
{
	authedredis cluster nodes | grep myself | cut -d' ' -f3 | cut -d',' -f2
}

function get_my_slave_address()
{
	NODE_ROLE=$(get_node_role)

	if [ "$NODE_ROLE" -neq "master" ]; then
		return 1
	fi

	MASTER_ID=$(authedredis cluster nodes | grep myself | cut -d' ' -f1 | cut -d',' -f2)
	SLAVE_ADDRESS=$(authedredis cluster node | grep "slave $MASTER_ID" | cut -d ' ' -f2 | cut -d'@' -f1)

	echo "$SLAVE_ADDRESS"
	return 0
}

function pingmember()
{
	if [ "$#" -lt 1 ]; then
		return 1
	fi

	IP="$1"
	authedredis -h $IP ping
}

function pingself() {
	authedredis ping
}

function get_member_pod_info()
{
	curl -s -H "Authorization: Bearer $TOKEN" \
		--cacert $CACERT \
		$K8S_HOST/api/v1/namespaces/$NAMESPACE/pods?labelSelector=$POD_FILTER_LABEL%3D$POD_FILTER_VALUE \
		| jq '.items[] | {name: .metadata.name, ip: .status.podIP, state: .status.phase, port: .spec.containers[0].ports[0].containerPort}'
}

function get_member_pod_ips()
{
	get_member_pod_info \
		| jq '.ip' \
		| tr -d '"'
}

function get_other_pod_host_addresses()
{
	get_member_pod_info \
		| jq 'select(.state == "Running")' \
		| jq '(.ip + ":" + (.port|tostring))' \
		| tr -d '"'
}

function get_running_member_pod_ips()
{
	get_member_pod_info \
		| jq 'select(.state == "Running")' \
		| jq '.ip'
}

function get_sts_info()
{
	curl -s -H "Authorization: Bearer $TOKEN" \
		--cacert $CACERT \
		$K8S_HOST/apis/apps/v1/namespaces/$NAMESPACE/statefulsets/$REDIS_STS_NAME \
		| jq '{name: .metadata.name, generation: .metadata.generation, replicas: .spec.replicas}'
}

function get_sts_generation()
{
	get_sts_info | jq '.generation'
}

function desired_sts_replicas()
{
	get_sts_info | jq '.replicas'
}

function ordinal_index()
{
	hostname | rev | cut -d- -f1 | rev
}

function node_cluster_state()
{
	authedredis cluster info | grep cluster_state | cut -d':' -f2
}

function node_is_clustered()
{
	STATE=$(node_cluster_state)

	if [[ "$STATE" == "ok"* ]]; then
		return 0
	fi

	return 1
}

function wait_for_node_ping()
{
	ITERATION=0
	until pingself
	do
		echo "redis is not ready yet"
		echo "waiting $WAIT_INTERVAL seconds, to retry ping"
		sleep $WAIT_INTERVAL
		ITERATION=$((ITERATION + 1))

		if (( $ITERATION >= $MAX_PING_COUNT )); then
			echo "exceeded maximum allowed pings"
			return 1
		fi
	done

	return 0
}

function wait_for_node_to_be_clustered()
{
	ITERATION=0
	while (( $ITERATION < $MAX_CLUSTER_CHECK_COUNT ))
	do
		node_is_clustered

		if [ "$?" == "0" ]; then
			return 0
		fi

		ITERATION=$((ITERATION + 1))
		echo "node is not clustered yet"
		echo "waiting $WAIT_INTERVAL seconds, to retry check"
		sleep $WAIT_INTERVAL
	done

	echo "exceeded maximum allowed cluster state checks"

	return 1
}

function pingmembers()
{
	FAILED=false
	while read -r MEMBER_IP ; do
		pingmember $MEMBER_IP
		
		if [ "$?" != "0" ]; then
			FAILED=true
			echo "Error could not ping $MEMBER_IP"
		fi
	done <<<$(get_member_pod_info | jq "select(.name == \"$(hostname)\" | not)" | jq '.ip' | tr -d '"')


	if [ "$FAILED" == "true" ]; then
		return 1
	fi

	return 0
}

function check_members_are_clustered()
{
	FAILED=false

	while read -r MEMBER ; do
		MEMBER_NAME=$(echo $MEMBER | cut -d':' -f1)
		MEMBER_IP=$(echo $MEMBER | cut -d':' -f2)
		STATE=$(authedredis -h $MEMBER_IP cluster info | grep cluster_state | cut -d':' -f2 | tr -d '[:space:]')
		
		echo "$MEMBER_NAME cluster_state: $STATE"

		if [ "$STATE" == "fail" ]; then
			FAILED=true
		fi
	done <<<$(get_member_pod_info | jq '"\(.name):\(.ip)"' | tr -d '"')

	if [ "$FAILED" == "true" ]; then
		return 1
	fi

	return 0
}

function wait_for_members_ping()
{
	ITERATION=0
	until pingmembers
	do
		echo "one or more redis instances are not responding to ping"
		echo "waiting $WAIT_INTERVAL seconds"
		sleep $WAIT_INTERVAL
		ITERATION=$((ITERATION + 1))

		if (( $ITERATION > $MAX_PING_COUNT )); then
			echo "exceeded maximum allowed pings"
			return 1
		fi
	done

	return 0
}

function wait_for_cluster_members_registered()
{
	ITERATION=0
	until check_members_are_clustered
	do
		echo "one or more redis instances have not joined a cluster"
		echo "waiting $WAIT_INTERVAL seconds"
		sleep $WAIT_INTERVAL
		ITERATION=$((ITERATION + 1))

		if (( $ITERATION > $MAX_CLUSTER_CHECK_COUNT )); then
			echo "exceeded maximum allowed cluster checks"
			return 1
		fi
	done

	return 0
}