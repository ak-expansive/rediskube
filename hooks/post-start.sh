#!/bin/bash

. $HOOK_ROOT/.lib/common.sh

function create_cluster()
{
	echo "HOSTS ARE"
	echo "== $(get_other_pod_host_addresses) || $POD_IP:$EXPOSED_REDIS_PORT =="
	# This container does not have an IP in the kubernetes API at this point
	# It's not finished spinning up
	# That's why we join other pod addresses to this one
	authedredis \
		--cluster create \
		--cluster-yes \
		$(get_other_pod_host_addresses | tr '\n' ' ') $POD_IP:$EXPOSED_REDIS_PORT \
		--cluster-replicas 1 

	return "$?"
}

function initialise()
{
	# Wait for redis to start serving; no guarantees in post-start hook
	wait_for_node_ping

	if [ "$?" != "0" ]; then
		echo "node is not responding to pings, tries: ($MAX_PING_COUNT)"
		exit 1
	fi

	# It's already in a cluster
	if [ "$(node_cluster_state)" == "ok" ]; then
		return 0
	fi

	# Ensure all nodes are reachable and responsive
	wait_for_members_ping

	MEMBER_PING_STATUS="$?"

	if [ "$MEMBER_PING_STATUS" != "0" ]; then
		echo "one or more members are could not be fixed, tries: ($MAX_CLUSTER_CHECKS_COUNT)"
		exit "$?"
	fi

	# Join all the nodes as cluster
	create_cluster

	if [ "$?" != "0" ]; then
		echo "failed to create cluster"
		exit 1
	fi

	# Ensure that the node is now in a clustered state before exiting
	wait_for_node_to_be_clustered

	NODE_CLUSTER_STATUS="$?"

	if [ "$NODE_CLUSTER_STATUS" != "0" ]; then
		echo "one or more members are not in a cluster, tries: ($MAX_CLUSTER_CHECKS_COUNT)"
	fi

	echo "nodes clustered successfully"
	exit "$NODE_CLUSTER_STATUS"
}

function main()
{
	STS_GENERATION=$(get_sts_generation)
	ORDINAL_INDEX=$(ordinal_index)
	FINAL_INDEX=$(($(desired_sts_replicas) - 1))

	# If STS_GENERATION == 1; then this is the first deployment of the cluster
	# If STS_GENERATION > 1; then it is an update to the existing cluster
	# STS_GENERATION should never be < 1
	if [ "$STS_GENERATION" == "1" ]; then

		node_is_clustered

		# this hook *could* be run multiple times by kubernetes
		# if the node is already in a clustred state then it is being run more than once
		# we should not attempt to 'recluster' it
		if [ "$?" == "0" ]; then
			echo "Node is already clustered."
			exit 0
		fi
		if [ "$ORDINAL_INDEX" == "$FINAL_INDEX" ]; then
			initialise
			exit "$?"
		fi

		exit 0
	fi
}

main
