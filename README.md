# Redis kube

A docker image designed to create a redis cluster within kubernetes with no manual input.

## Base image

This is based entirely on the official redis image (See Dockerfile for version), and is designed to change a minimal amount when compared to the official image (it uses the same entrypoint and configs for example).

We add some extra scripts to the base image which intergrate with a kubernetes API to figure how to orchestrate a statefulset of containers into a single redis cluster.

## Sentinel

This does not use sentinel and instead relies upon official redis cluster functionality.

