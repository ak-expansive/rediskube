#!/bin/bash

kubectl apply -f ./rbac.yaml
kubectl apply -f ./configmaps.yaml
kubectl apply -f ./services.yaml
kubectl apply -f ./sts.yaml
kubectl rollout status -f ./sts.yaml