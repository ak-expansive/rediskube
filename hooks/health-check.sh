#!/bin/bash

. $HOOK_ROOT/.lib/common.sh

function main()
{
	# If the node is clustered then we're healthy
	node_is_clustered
	exit "$?"
}

main
