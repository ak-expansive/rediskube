#!/bin/bash

. $HOOK_ROOT/.lib/common.sh

function main()
{
	ORDINAL_INDEX=$(ordinal_index)
	FINAL_INDEX=$(($(desired_sts_replicas) - 1))

	# If this is the last node, we're not ready until it is successfully clustered
	if [ "$ORDINAL_INDEX" == "$FINAL_INDEX" ]; then
		check_members_are_clustered
		exit "$?"
	else
	# If this is not the last node we just need redis to be running
		pingself

		exit "$?"
	fi
}

main
