#!/bin/bash

. $HOOK_ROOT/.lib/common.sh

function failover_to_slave() {
	SLAVE_ADDRESS=$(get_my_slave_address)

	pingmember "$SLAVE_ADDDRESS"

	if [ "$?" -eq "1" ]; then
		echo "Could not ping slave@$SLAVE_ADDRESS"
		return 1
	fi

	FAILOVER_RESPONSE=$(authedredis -h $SLAVE_ADDRESS cluster failover)

	if [ "$FAILOVER_RESPONSE" -eq "OK" ]; then
		return 0
	fi

	return 1
}

function main() {
	node_is_clustered

	NODE_CLUSTER_STATUS="$?"

	if [ "$NODE_CLUSTER_STATUS" != "0" ]; then
		echo "This node is not part of a cluster; shutting down normally"
		exit 0
	fi

	NODE_ROLE=$(get_node_role)

	if [ "$NODE_ROLE" -eq "master" ]; then
		failover_to_slave
		exit "$?"
	elif [ "$NODE_ROLE" -eq "slave" ]; then
		# It's a slave do nothing; the master should deal with it
		echo "Shutting down slave, no resharding actions necessary"
		exit 0
	fi

	echo "Unknown NODE role '$NODE_ROLE'"
	exit 1
}

main